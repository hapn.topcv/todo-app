import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './Screen/Home';
import Child from './Component/Child';
import ListScreen  from './Screen/ListScreen';

const MainStack = createStackNavigator({
    Home: {
        screen: Home,
        navigationOptions: ({
            headerShown: false
        })
    },
    Child: {
        screen: ListScreen,
        navigationOptions:({
            headerShown: false
        })
        
    }
});
const App = createAppContainer(MainStack);

export default App;
