import React from 'react';
import {
	View,
	Image,
	Text,
	TextInput,
	StyleSheet,
	ToastAndroid,
	Alert,
	KeyboardAvoidingView
} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Home extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			email: '',
			password: ''
		};
	}

	_login = () => {
		// if (this.state.email == 'ngocha' && this.state.password == '123456') {
			this.props.navigation.navigate('Child');
		// }
		// else {
		// 	Alert.alert("Sai mật khẩu hoặc tài khoản", "Mời nhập lại", [{ text: 'OK', onPress: () => console.log('OK Pressed') }]);
		// }
	};
	render() {
		return (
			<View style={styles.main} >
				<View style={styles.head}>
					<Image
						style={styles.img}
						source={require('../assets/list.png')}
					/>
					<Text style={styles.t1}>
						Ghi chú cá nhân
                </Text>
				</View>
				<View style={styles.login}>
					<Text
						style={styles.login_text}>Email
                    </Text>
					<TextInput
						value={this.state.email}
						onChangeText={(t) => {
							this.setState({
								email: t
							});
						}}
						style={styles.input}
						placeholder='Email đăng nhập'>
					</TextInput>
					<Text
						style={styles.login_text}>Mật khẩu
                    </Text>
					<TextInput
						value={this.state.password}
						onChangeText={(t) => {
							this.setState({
								password: t
							});
						}}
						style={styles.input}
						placeholder='Mật khẩu' secureTextEntry={true}>
					</TextInput>
					<TouchableOpacity
						onPress={this._login}
						style={styles.submitButton}
						color='#FFF'
					>
						<Text style={{
							fontSize: 16,
							fontWeight: 'bold',
							color: '#FFF'
						}} >Đăng nhập</Text>
					</TouchableOpacity>

				</View>
			</View>
		);
	}

}

export default Home;

const styles = StyleSheet.create({
	main: {
		flex: 1,
		flexDirection: 'column',
	},
	head: {
		alignItems: 'center'
	},
	img: {
		marginTop: 75,
		width: 100,
		height: 100,
		transform: [{ rotate: '15deg' }],
		marginBottom: 20
	},
	t1: {
		fontSize: 25,
		fontWeight: 'bold',
	},
	login: {
		marginTop: 10,
		marginLeft: 20,
		marginRight: 20,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
		backgroundColor: '#fff',
		borderRadius: 8
	}
	,
	login_text: {
		marginTop: 37,
		marginLeft: 27,
		fontWeight: 'bold',
		fontSize: 16,

	},
	input: {
		paddingLeft: 16,
		borderWidth: 1,
		borderColor: '#CACACA',
		marginRight: 28,
		marginLeft: 28,
		borderRadius: 4,
		margin: 5
	},
	submitButton: {
		borderRadius: 4,
		marginLeft: 28,
		marginRight: 28,
		marginTop: 42,
		marginBottom: 35,
		height: 42,
		paddingTop: 10,
		alignItems: 'center',
		backgroundColor: '#41DAB5',
	}
});