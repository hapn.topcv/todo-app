import React from 'react';
import { AsyncStorage, View, FlatList, Text, SafeAreaView, Image, StyleSheet, Modal, Button, Alert, TouchableOpacity, ScrollView, Dimensions, ToastAndroid, TextInput, TouchableWithoutFeedback } from 'react-native';
import Child from '../Component/Child';

const DATA = [{ index: '1', detail: 'aaa' }];
class ListScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            newTodoDetail: '',
        };
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    componentDidMount() {
        this._loadData();
    }

    _loadData = async () => {
        // let list = await AsyncStorage.getItem("store");
        // todos.reload(JSON.parse(list));
    };



    renderList() {
        return (
            <SafeAreaView>
                <FlatList
                    data={DATA}
                    renderItem={({ item }) =>
                        <Child item={item} />
                    }
                    keyExtractor={item => item.index}
                >
                </FlatList>
            </SafeAreaView>
        );
    }


    render() {
        return (
            <View style={{
                flex: 1,
            }}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            this.setModalVisible(false);
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'rgba(52, 52, 52, 0.8)',
                                borderWidth: 1
                            }}
                        >
                            <TouchableWithoutFeedback>
                                <View style={{
                                    backgroundColor: '#fff',
                                    paddingTop: 20,
                                    paddingBottom: 20,
                                    borderRadius: 8,
                                    shadowColor: "#000",
                                    shadowOffset: {
                                        width: 0,
                                        height: 2,
                                    },
                                    shadowOpacity: 0.25,
                                    shadowRadius: 3.84,
                                    elevation: 5,
                                    backgroundColor: '#fff',
                                    position: 'absolute',
                                    width: Math.round(Dimensions.get('window').width) * 0.9,
                                    left: Math.round(Dimensions.get('window').width) * 0.05,
                                    top: Math.round(Dimensions.get('window').height) * 0.4
                                }}>
                                    <View>
                                        <TextInput
                                            value={this.state.newTodoDetail}
                                            onChangeText={(text) => {
                                                this.setState({
                                                    newTodoDetail: text
                                                });
                                            }}
                                            style={styles.input}
                                            placeholder='Ghi chú mới' />
                                        <View style={{
                                            flexDirection: 'row',
                                            justifyContent: 'flex-end'
                                        }}>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setModalVisible(false);
                                                }}>
                                                <Text
                                                    style={{
                                                        marginRight: 20,
                                                        width: 100,
                                                        textAlign: 'center',
                                                        marginTop: 10,
                                                        backgroundColor: '#626262',
                                                        color: '#fff',
                                                        height: 30,
                                                        paddingTop: 5,
                                                        borderRadius: 5
                                                    }}
                                                >Hủy</Text>
                                            </TouchableOpacity>
                                            <TouchableOpacity
                                                onPress={() => {
                                                    this.setState({
                                                        newTodoDetail: ''
                                                    });
                                                    this.setModalVisible(false);
                                                }}>
                                                <Text
                                                    style={{
                                                        marginRight: 28,
                                                        width: 100,
                                                        textAlign: 'center',
                                                        marginTop: 10,
                                                        backgroundColor: '#41DAB5',
                                                        color: '#fff',
                                                        height: 30,
                                                        paddingTop: 5,
                                                        borderRadius: 5
                                                    }}
                                                >Lưu</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>
                <View style={styles.head}>
                    <Text
                        style={styles.text_head}
                    > Danh sách ghi chú</Text>
                    <View style={{
                        position: "absolute",
                        right: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => {
                                this.setModalVisible(true);
                            }}>
                            <Image
                                style={styles.img}
                                source={require('../assets/pen.png')}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                <View>
                    <Text style={styles.tk}>Thống kê</Text>
                    <Text style={styles.done}>Đã hoàn thành:</Text>
                    <Text style={styles.done}>Chưa hoàn thành:</Text>
                    <Text style={styles.ds}>Danh sách</Text>
                </View>
                {this.renderList()}
            </View>
        );
    }


}

export default ListScreen;

const styles = StyleSheet.create({
    head: {
        marginTop: 10,
    },
    img: {
        width: 30,
        height: 30,
    },
    text_head: {
        fontWeight: 'bold',
        fontSize: 20,
        // borderWidth:1,
        textAlign: 'center',
        paddingTop: 5,
        height: 40
    },
    tk: {
        fontWeight: 'bold',
        fontSize: 16,
        marginLeft: 25
    },
    done: {
        marginLeft: 29,
        fontSize: 16,
    },
    ds: {
        marginTop: 10,
        fontWeight: 'bold',
        fontSize: 16,
        marginLeft: 25
    },
    container: {
        flex: 1,
        justifyContent: 'center',
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'grey',
    },
    innerContainer: {
        alignItems: 'center',
    },
    input: {
        paddingLeft: 16,
        borderWidth: 1,
        borderColor: '#CACACA',
        marginRight: 28,
        marginLeft: 28,
        borderRadius: 4,
        margin: 5
    }
});;