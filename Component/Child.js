import React from 'react';
import { View, Text, CheckBox, StyleSheet, Modal, Dimensions, TextInput, Alert, TouchableWithoutFeedback, TouchableOpacity } from 'react-native';

class Child extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
			checked: false,
			modalVisible: false,
			newTodoDetail: ''
		};
	}

	componentDidMount() {
		this.setState({
			newTodoDetail: this.props.item.detail
		});
	}

	setModalVisible(visible) {
		this.setState({ modalVisible: visible });
	}

	render() {
		return (
			<View style={styles.main}>
				<View style={styles.top}>
					<Text
						style={[styles.text_top, {
							textDecorationLine: this.state.checked ? 'line-through' : 'none',
						}]}
					>
                        {this.props.item.detail}
					</Text>
					<View style={styles.check_box}>
						<CheckBox
							value={this.state.checked}
							onValueChange={() => {
								this.setState({
									checked: this.state.checked ? false : true
								});
							}}
						>
						</CheckBox>
					</View>
				</View>
				<View style={styles.bot}>
					<TouchableOpacity
						onPress={() => {
							this.setModalVisible(true);
						}}
					>
						<Text style={styles.bot_r}>
							Sửa
					</Text>
					</TouchableOpacity>
					<TouchableOpacity
						onPress={() => {
						
						}}
					>
						<Text style={styles.bot_l}>
							Xóa
					</Text>
					</TouchableOpacity>
				</View>
				<Modal
					animationType="slide"
					transparent={true}
					visible={this.state.modalVisible}
					onRequestClose={() => {
						Alert.alert('Modal has been closed.');
					}}>
					<TouchableWithoutFeedback
						onPress={() => {
							this.setModalVisible(false);
						}}
					>
						<View
							style={{
								flex: 1,
								backgroundColor: 'rgba(52, 52, 52, 0.8)',
								borderWidth: 1
							}}
						>
							<TouchableWithoutFeedback>
								<View style={{
									backgroundColor: '#fff',
									paddingTop: 20,
									paddingBottom: 20,
									borderRadius: 8,
									shadowColor: "#000",
									shadowOffset: {
										width: 0,
										height: 2,
									},
									shadowOpacity: 0.25,
									shadowRadius: 3.84,
									elevation: 5,
									backgroundColor: '#fff',
									position: 'absolute',
									width: Math.round(Dimensions.get('window').width) * 0.9,
									left: Math.round(Dimensions.get('window').width) * 0.05,
									top: Math.round(Dimensions.get('window').height) * 0.4
								}}>
									<View>
										<TextInput
											value={this.state.newTodoDetail}
											onChangeText={(text) => {
												this.setState({
													newTodoDetail: text
												});
											}}
											style={styles.input}
											placeholder='Ghi chú mới' />
										<View style={{
											flexDirection: 'row',
											justifyContent: 'flex-end'
										}}>
											<TouchableOpacity
												onPress={() => {
													this.setModalVisible(false);
												}}>
												<Text
													style={{
														marginRight: 20,
														width: 100,
														textAlign: 'center',
														marginTop: 10,
														backgroundColor: '#626262',
														color: '#fff',
														height: 30,
														paddingTop: 5,
														borderRadius: 5
													}}
												>Hủy</Text>
											</TouchableOpacity>
											<TouchableOpacity
												onPress={() => {
													this.setModalVisible(false);
												}}>
												<Text
													style={{
														marginRight: 28,
														width: 100,
														textAlign: 'center',
														marginTop: 10,
														backgroundColor: '#41DAB5',
														color: '#fff',
														height: 30,
														paddingTop: 5,
														borderRadius: 5
													}}
												>Lưu</Text>
											</TouchableOpacity>
										</View>
									</View>
								</View>
							</TouchableWithoutFeedback>
						</View>
					</TouchableWithoutFeedback>
				</Modal>
			</View >
		);
	}
}

export default Child;

const styles = StyleSheet.create({
	main: {
		marginTop: 10,
		marginLeft: 21,
		marginRight: 21,
		shadowColor: "#000",
		shadowOffset: {
			width: 0,
			height: 2,
		},
		shadowOpacity: 0.25,
		shadowRadius: 3.84,
		elevation: 5,
		backgroundColor: '#fff',
		borderRadius: 8,
		marginBottom: 5
	},
	top: {
		flexDirection: 'row',
	},
	text_top: {

		marginTop: 6,
		flex: 6,
		marginLeft: 18,
		fontSize: 16,
	},
	check_box: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'flex-end',
		marginTop: 6,
		marginRight: 6
	},
	bot: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginTop: 12,
		marginBottom: 12
	},
	bot_r: {
		borderRadius: 4,
		width: 150,
		height: 42,
		paddingTop: 10,
		alignItems: 'center',
		backgroundColor: '#41DAB5',
		paddingLeft: 60,
		color: '#fff',
		fontWeight: 'bold'
	},
	bot_l: {
		borderRadius: 4,
		width: 150,
		height: 42,
		paddingTop: 10,
		alignItems: 'center',
		backgroundColor: '#EC5D5D',
		marginLeft: 8,
		paddingLeft: 60,
		color: '#fff',
		fontWeight: 'bold'
	},
	input: {
		paddingLeft: 16,
		borderWidth: 1,
		borderColor: '#CACACA',
		marginRight: 28,
		marginLeft: 28,
		borderRadius: 4,
		margin: 5
	}
});